// Package token contains types that the Lexer
// will parse a program into
package token

// Type is the type of a token; one of the values below
type Type string

// Token represents something that the lexer will parse some text into
type Token struct {
	Type    Type
	Literal string
}

const (
	// ILLEGAL represents something in the program that was not expected
	ILLEGAL = "ILLEGAL"

	// EOF represents the end of the file
	EOF = "EOF"

	// IDENT represents a user-defined identifier, like a variable
	IDENT = "IDENT"

	// INT represents a number
	INT = "INT"

	// ASSIGN represents an assignment
	ASSIGN = "="

	// PLUS represents a + sign
	PLUS = "+"

	// MINUS represents a - sign
	MINUS = "="

	// BANG represents a explamation mark (!)
	BANG = "!"

	// ASTERISK represents a * sign
	ASTERISK = "*"

	// SLASH represents a / sign
	SLASH = "/"

	// LT represents a less-than sign
	LT = "<"

	// GT represents a greater-than sign
	GT = ">"

	// COMMA represents a seperator
	COMMA = ","

	// SEMICOLON represents the end of a statement
	SEMICOLON = ";"

	// LPAREN represents a left parenthesis
	LPAREN = "("

	// RPAREN represents a right parenthesis
	RPAREN = ")"

	// LBRACE represents a right bracket
	LBRACE = "{"

	// RBRACE represents a right bracket
	RBRACE = "}"

	// FUNCTION represents the start of a function
	FUNCTION = "FUNCTION"

	// LET represents a new variable assignment
	LET = "LET"

	// TRUE represents `true`
	TRUE = "TRUE"

	// FALSE represents `false`
	FALSE = "FALSE"

	// IF represents the start of an "if" statement
	IF = "IF"

	// ELSE represents the alternate to an "if" statement
	ELSE = "ELSE"

	// RETURN represents an explicit return statement
	RETURN = "RETURN"

	// EQ represents an equality comparison
	EQ = "=="

	// NOTEQ represents an inverse equality comparison
	NOTEQ = "!="
)

var keywords = map[string]Type{
	"fn":     FUNCTION,
	"let":    LET,
	"true":   TRUE,
	"false":  FALSE,
	"if":     IF,
	"else":   ELSE,
	"return": RETURN,
}

// LookupIdent returns the correct Type for an identifier, based on the list of keywords
func LookupIdent(ident string) Type {
	if tok, ok := keywords[ident]; ok {
		return tok
	}

	return IDENT
}
