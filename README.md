# Monkey Interpreter

[![build status](https://gitlab.com/alexlafroscia/monkey-interpreter/badges/master/build.svg)](https://gitlab.com/alexlafroscia/monkey-interpreter/commits/master)
[![coverage report](https://gitlab.com/alexlafroscia/monkey-interpreter/badges/master/coverage.svg)](https://gitlab.com/alexlafroscia/monkey-interpreter/commits/master)

An interpreter for Monkey (a language created for [Writing an Interpreter in Go][writing-an-interpreter], written in Go.

[writing-an-interpreter]: https://interpreterbook.com
